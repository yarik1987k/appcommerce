import React, { useState, useEffect, useRef } from 'react';
import { View, StyleSheet } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from '../screens/HomeScreen';
import CategoryScreen from '../screens/CategoryScreen';
import CartScreen from '../screens/CartScreen';
import CheckoutScreen from '../screens/CheckoutScreen'; 
import FloatingCart from '../components/FloatingCart';
import OnBoarding from '../screens/OnBoarding';
import OrderScreen from '../screens/OrderScreen'; 
import MyOrdersScreen from '../screens/MyOrdersScreen';
import MyAddressScreen from '../screens/MyAddressScreen';
import { useCurrentRouteName } from './routeHook';

const Stack = createNativeStackNavigator();

function AppNavigator({ navigationRef }) {
  const [cart, setCart] = useState([]); 
  const currentRouteName = useCurrentRouteName(navigationRef);

  const addToCart = (product) => {
    setCart((prevCart) => {
      const existingProductIndex = prevCart.findIndex((item) => item.title === product.title);

      if (existingProductIndex > -1) {
        // Product already in the cart, update its quantity
        const updatedCart = [...prevCart];
        updatedCart[existingProductIndex] = {
          ...updatedCart[existingProductIndex],
          quantity: updatedCart[existingProductIndex].quantity + product.quantity
        };
        return updatedCart;
      } else {
        // Product not in the cart, add it
        return [...prevCart, { ...product }];
      }
    });
  };


  const updateQuantity = (product, quantity) => {
    if (quantity === 0) {
      setCart((prevCart) => prevCart.filter((item) => item.title !== product.title));
    } else {
      setCart((prevCart) =>
        prevCart.map((item) =>
          item.title === product.title ? { ...item, quantity } : item
        )
      );
    }
  };

  const removeFromCart = (id) => {
    setCart((prevCart) => prevCart.filter((item) => item.title !== id));
  };

  return (
    <View style={styles.container}> 
      <Stack.Navigator initialRouteName="On Boarding">
        <Stack.Screen
                  name="OnBoarding"
                  options={{ headerShown: false }}
        >
          {props => <OnBoarding {...props} cart={cart} addToCart={addToCart} />}
        </Stack.Screen>
        <Stack.Screen 
          name="Home"
          options={{ headerShown: false }}
        >
          {props => <HomeScreen {...props} cart={cart} addToCart={addToCart} />}
        </Stack.Screen>
        <Stack.Screen 
          name="Category"
          options={{ headerShown: false }}
        >
          {props => <CategoryScreen  updateQuantity={updateQuantity} {...props} cart={cart} addToCart={addToCart} />}
        </Stack.Screen>
        <Stack.Screen 
          name="Cart"
          options={{ headerShown: false }}
        >
          {props => <CartScreen {...props} cart={cart} updateQuantity={updateQuantity} removeFromCart={removeFromCart} />}
        </Stack.Screen>
        <Stack.Screen 
          name="Checkout"
          options={{ headerShown: false }}
        >
          {props => <CheckoutScreen {...props} cart={cart} />}
        </Stack.Screen>
        <Stack.Screen 
          name="OrderScreen"
          options={{ headerShown: false }}
        >
          {props => <OrderScreen {...props} cart={cart} />}
        </Stack.Screen>
        <Stack.Screen 
          name="MyOrders"
          options={{ headerShown: false }}
        >
          {props => <MyOrdersScreen {...props} />}
        </Stack.Screen>       
        <Stack.Screen 
          name="MyAddress"
          options={{ headerShown: false }}
        >
          {props => <MyAddressScreen {...props} />}
        </Stack.Screen>    
      </Stack.Navigator>
      {currentRouteName !== 'Cart' && currentRouteName !== 'OnBoarding' && currentRouteName !== 'Checkout' && currentRouteName !== 'OrderScreen' && <FloatingCart cart={cart} />}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default AppNavigator;
