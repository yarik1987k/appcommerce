import { useState, useEffect } from 'react';

export const useCurrentRouteName = (navigationRef) => {
  const [currentRouteName, setCurrentRouteName] = useState();

  useEffect(() => {
    if (!navigationRef.current) {
      return;
    }

    const listener = () => {
      setCurrentRouteName(navigationRef.current.getCurrentRoute()?.name);
    };

    const unsubscribeFocus = navigationRef.current?.addListener('state', listener);

    return () => {
      unsubscribeFocus();
    };
  }, [navigationRef]);

  console.log('currentRouteName - component', navigationRef.current?.getCurrentRoute()?.name);
  return currentRouteName;
};
