// FloatingCart.js 
import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';


const FloatingCart = ({ cart }) => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  // Calculate the total quantity of items in the cart
  const totalItems = cart.reduce((total, item) => total + item.quantity, 0);

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => navigation.navigate('Cart')}
    >
      <Text style={styles.text}>{t('buttons.goToCart')} ({totalItems})</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    backgroundColor: 'blue',
    borderRadius: 50,
    padding: 15,
    zIndex: 1,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
  },
  
});

export default FloatingCart;
