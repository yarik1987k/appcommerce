// languageDetector.js
import AsyncStorage from '@react-native-async-storage/async-storage';

const LANGUAGE_KEY = 'appLanguage';

const languageDetector = {
  type: 'languageDetector',
  async: true,
  init: () => {},
  detect: async (callback) => {
    try {
      const language = await AsyncStorage.getItem(LANGUAGE_KEY);
      if (language) {
        return callback(language);
      } else {
        // If no language is set, default to English
        return callback('en');
      }
    } catch (error) {
      console.error('Error detecting language:', error);
      return callback('en');
    }
  },
  cacheUserLanguage: async (language) => {
    try {
      await AsyncStorage.setItem(LANGUAGE_KEY, language);
    } catch (error) {
      console.error('Error caching user language:', error);
    }
  }
};

export default languageDetector;
