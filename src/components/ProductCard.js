import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';
import AddToCartIcon from '../assets/icons/addToCart.svg';

const ProductCard = ({ item, quantity, increaseQuantity, decreaseQuantity, handleAddToCart }) => {
  const { t } = useTranslation();

  const handleDecreaseQuantity = () => {
    if (quantity > 0) {
      decreaseQuantity(item.slug);
    }
  };

  return (
    <View style={styles.productCard}>
      <View style={styles.productCardInner}>
      <Image source={{ uri: item.image }} style={styles.productImage} />
      <Text style={styles.productText}>{item.title}</Text>
      <Text style={styles.productPrice}>₪ {item.price}</Text>
      {item?.label?.name && (
        <Text style={styles.productLabel}>{item.label.name}</Text>
      )}
      <View style={styles.quantityContainer}>
        <TouchableOpacity onPress={handleDecreaseQuantity} style={styles.quantityButton}>
          <Text style={styles.quantityButtonText}>-</Text>
        </TouchableOpacity>
        <Text style={styles.quantityText}>{quantity}</Text>
        <TouchableOpacity onPress={() => increaseQuantity(item.slug)} style={styles.quantityButton}>
          <Text style={styles.quantityButtonText}>+</Text>
        </TouchableOpacity>
      </View> 
      <TouchableOpacity style={styles.addButton} onPress={() => handleAddToCart({ ...item, quantity })}>
        <View style={styles.addButtonContent}>
          <Text><AddToCartIcon width={16} height={16} /> </Text>{/* Adjust the size as needed */}
          <Text style={styles.addButtonText}>{t('buttons.addToCart')}</Text>
        </View>
      </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  addButtonContent:{
    flexDirection: 'row',
      alignItems: 'center',
    justifyContent: 'center'
  },
  addButton: {
    borderTopWidth:1,
    paddingTop:10,
    borderColor: '#EBEBEB',
    marginVertical:10, 
    width: '100%'
  },
  addButtonText: {
    color: '#010101',
    fontSize: 10,
    fontWeight: 'bold',
  },
  productCard: {
    width: '50%', 
   padding: 10
  },
  productCardInner: {
    padding: 10, 
    borderWidth: 1,
    borderColor: "#E2E2E2",
    borderRadius: 18,
    backgroundColor: "#FFFFFF",
    alignItems: 'center',
  },
  productImage: {
    width: '100%',
    aspectRatio: 1, // Ensures the image is always square
    marginBottom: 10,

  },
  productText: {
    fontSize: 12,
    paddingHorizontal:20, 
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    height: 50
  },
  productPrice: {
    fontSize: 12,
    color: 'gray',
  },
  productLabel: {
    fontSize: 8,
    textAlign: "center",
    color: '#7C7C7C',
    marginBottom: 10,
    marginTop:10,
    backgroundColor: "#EBEBEB",
    borderRadius: 10,
    paddingTop:5,
    paddingBottom:5,
    paddingHorizontal: 10
  },
  quantityContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    width:'100%'
  },
  quantityButton: {
    borderWidth: 1,
    borderColor: '#EBEBEB',
    padding: 5,
    width: 30,
    height:30,
    borderRadius: 5,
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  },
  quantityButtonText: {
    fontSize: 12,
    lineHeight: 13
  },
  quantityText: {
    marginHorizontal: 10,
    fontSize: 16,
  },
});

export default ProductCard;
