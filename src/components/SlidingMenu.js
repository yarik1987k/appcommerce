// src/components/SlidingMenu.js
import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const SlidingMenu = ({ closeMenu }) => {
  const navigation = useNavigation();

  return (
    <View style={styles.menuContainer}>
      <TouchableOpacity onPress={() => { navigation.navigate('MyOrders'); closeMenu(); }} style={styles.menuItem}>
        <Text style={styles.menuItemText}>My Orders</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => { navigation.navigate('MyAddress'); closeMenu(); }} style={styles.menuItem}>
        <Text style={styles.menuItemText}>My Address</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  menuContainer: {
    flex: 1,
    backgroundColor: 'white',
    padding: 20,
    width: 250,
  },
  menuItem: {
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  menuItemText: {
    fontSize: 18,
  },
});

export default SlidingMenu;
