import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import i18n from '../../i18n';

const LanguageSwitcher = () => {
  const [selectedLanguage, setSelectedLanguage] = useState(i18n.language);

  const languages = [
    { label: 'English', value: 'en', flag: require('../assets/flags/en.jpg') },
    { label: 'Hebrew', value: 'he', flag: require('../assets/flags/he.jpg') },
    { label: 'Russian', value: 'ru', flag: require('../assets/flags/ru.jpg') },
  ];

  useEffect(() => {
    const handleLanguageChanged = (lang) => setSelectedLanguage(lang);
    i18n.on('languageChanged', handleLanguageChanged);
    return () => {
      i18n.off('languageChanged', handleLanguageChanged);
    };
  }, []);

  const changeLanguage = (lang) => {
    setSelectedLanguage(lang);
    i18n.changeLanguage(lang);
  };

  const currentLanguage = languages.find(lang => lang.value === selectedLanguage);

  return (
    <View style={styles.container}>
      <RNPickerSelect
        onValueChange={(value) => changeLanguage(value)}
        items={languages.map(lang => ({
          label: lang.label,
          value: lang.value,
          key: lang.value,
        }))}
        value={selectedLanguage}
        placeholder={{}}
        style={pickerSelectStyles}
        useNativeAndroidPickerStyle={false}
        Icon={() => {
          return (
            <View style={styles.iconContainer}>
              <Image source={currentLanguage?.flag} style={styles.flag} />
            </View>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center', 
    justifyContent: 'flex-end', // Aligns items to the right
    width: '100%'
  },
  flag: {
    width: 24,
    height: 16,
    marginRight: 10,
  },
  iconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center', 
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    borderWidth: 0,
    color: 'transparent', // Hide the text
    paddingRight: 40, // to ensure the text is never behind the icon
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputAndroid: {
    fontSize: 16,
    borderWidth: 0,
    color: 'transparent', // Hide the text
    paddingRight: 30, // to ensure the text is never behind the icon
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default LanguageSwitcher;
