// src/components/CustomHeader.js
import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';  
import LanguageSwitcher from './LanguageSwitcher'; // Adjust the import path as needed
import ArrowBack from '../assets/icons/arrowBack.svg';
import MenuIcon from '../assets/icons/menu.svg';

const { width } = Dimensions.get('window');

const CustomHeader = ({ title }) => {
  const navigation = useNavigation();
  const route = useRoute();
  const isHomeScreen = route.name === 'Home';  
  return (
    
      <View style={isHomeScreen ? styles.headerContainerEnd : styles.headerContainer}>
        {!isHomeScreen && (
          <TouchableOpacity onPress={() => navigation.goBack()} style={styles.backButton}>
            <ArrowBack width={14} height={14} />
          </TouchableOpacity>
        )}
        <View style={styles.titleContainer}>
          <Text>{title}</Text>
        </View>
        <View style={styles.languageSwitcherContainer}>
          <LanguageSwitcher />
        </View> 
      </View>
       
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#f8f8f8', // Adjust the background color as needed
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
    zIndex: 2,
    width: '100%',
    height: 60,
    marginTop: 20,
    paddingHorizontal: 16,
  },
  headerContainerEnd: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#f8f8f8', // Adjust the background color as needed
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
    zIndex: 2,
    width: '100%',
    height: 60,
    marginTop: 20,
    paddingHorizontal: 16,
  },
  titleContainer: {
    width: '33.33%',
    textAlign: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  backButton: {},
  icon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex: 1,
    textAlign: 'center',
  },
  languageSwitcherContainer: {
    width: '5%',
  },
  menuButton: {
    marginLeft: 'auto',
  },
  menuContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: width,
    height: '100%',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: -2, height: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 5,
    zIndex: 1000,
    padding: 16,
  },
  closeButton: {
    alignSelf: 'flex-end',
  },
  closeButtonText: {
    fontSize: 16,
    color: '#007AFF',
  },
  menuItem: {
    fontSize: 18,
    paddingVertical: 16,
  },
});

export default CustomHeader;
