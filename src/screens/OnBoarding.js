//OnBoarding.js
import React from 'react';
import { View, Text, StyleSheet, ImageBackground, TouchableOpacity, Dimensions } from 'react-native';
import { useTranslation } from 'react-i18next';

const { width, height } = Dimensions.get('window');

function OnBoarding({ navigation }) {
    const { t } = useTranslation();
  return (
    <ImageBackground 
      source={require('../assets/images/onboarding.png')} // Change to your background image path
      style={styles.background}
    >
      <View style={styles.container}>
        {/* Add your onboarding content here */}
        <Text style={styles.title}>{t('onboarding.title')}</Text>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Home')}>
          <Text style={styles.buttonText}>{t('buttons.getStarted')}</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  background: {
    width: width,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 35,
    color: '#fff',
    marginBottom: 20,
    textAlign: 'center'
  },
  button: {
    backgroundColor: '#ff7f50',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  buttonText: {
    fontSize: 18,
    color: '#fff',
  },
});

export default OnBoarding;
