import React, { useState, useRef } from 'react';
import { View, Text, FlatList, StyleSheet, Animated } from 'react-native';
import ProductCard from '../components/ProductCard';
import { useTranslation } from 'react-i18next';
import PRODUCTS from '../data/products.json';
import CustomHeader from '../components/CustomHeader';

const numColumns = 2; 
function CategoryScreen({ route, addToCart, updateQuantity }) {
  const { t } = useTranslation();

  const { categoryId } = route.params;
  const products = PRODUCTS.filter(product =>
    product.categories.some(category => category.id === categoryId)
  );
  const categoryName = t(`categoriesData.${categoryId}`);

  const initialQuantities = products.reduce((acc, product) => {
    acc[product.slug] = 1;
    return acc;
  }, {});

  const [quantities, setQuantities] = useState(initialQuantities);
  const [headerVisible, setHeaderVisible] = useState(true);
  const scrollY = useRef(new Animated.Value(0)).current;
  const headerTranslateY = useRef(new Animated.Value(0)).current;

  const increaseQuantity = (slug) => {
    setQuantities({ ...quantities, [slug]: quantities[slug] + 1 });
  };

  const decreaseQuantity = (slug) => {
    const newQuantity = quantities[slug] - 1;
    setQuantities({ ...quantities, [slug]: newQuantity });
  };

  const handleAddToCart = (item) => {
    const quantity = quantities[item.slug];
    if (quantity > 0) {
      addToCart({ ...item, quantity });
    }
  };

  const renderProduct = ({ item }) => (
    <ProductCard
      item={item}
      quantity={quantities[item.slug]}
      increaseQuantity={increaseQuantity}
      decreaseQuantity={decreaseQuantity}
      handleAddToCart={handleAddToCart}
    />
  );

  const handleScroll = Animated.event(
    [{ nativeEvent: { contentOffset: { y: scrollY } } }],
    {
      useNativeDriver: true,
      listener: (event) => {
        const offsetY = event.nativeEvent.contentOffset.y;
        if (offsetY > 50 && headerVisible) {
          setHeaderVisible(false);
          Animated.timing(headerTranslateY, {
            toValue: -60, // Move header up by its height
            duration: 300,
            useNativeDriver: true,
          }).start();
        } else if (offsetY <= 50 && !headerVisible) {
          setHeaderVisible(true);
          Animated.timing(headerTranslateY, {
            toValue: 0, // Move header back to its original position
            duration: 300,
            useNativeDriver: true,
          }).start();
        }
      },
    }
  );

  return (
    <View style={styles.container}>
      <CustomHeader title={categoryName}/>  
      <Animated.FlatList
        contentContainerStyle={{ paddingTop: 0 }} // Adding top padding to create space for the header
        data={products}
        key={`flatlist-${numColumns}`}
        keyExtractor={(item) => item.slug}
        numColumns={numColumns}
        renderItem={renderProduct}
        columnWrapperStyle={{ justifyContent: 'space-between' }}
        onScroll={handleScroll}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerContainer: {
    height: 60,
    backgroundColor: '#ffffff',
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    position: 'absolute', // Ensure header is on top
    top: 80,
    zIndex: 1, // Ensure header is above other content
  },
  title: {
    fontSize: 16,
  },
});

export default CategoryScreen;
