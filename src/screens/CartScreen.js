import React from 'react';
import { View, Text, FlatList, Image, StyleSheet, TouchableOpacity } from 'react-native';
import TrashIcon from '../assets/icons/TrashIcon.svg'; // Ensure this path is correct
import { useTranslation } from 'react-i18next';
import CustomHeader from '../components/CustomHeader';
function CartScreen({ cart, updateQuantity, removeFromCart, navigation }) {
  const { t } = useTranslation();

  // Function to calculate the total cost
  const calculateTotalCost = () => {
    return cart.reduce((total, item) => total + parseFloat(item.price) * item.quantity, 0).toFixed(2);
  };



  const renderCartItem = ({ item }) => {
    const increaseQuantity = () => updateQuantity(item, item.quantity + 1);
    const decreaseQuantity = () => {
      const newQuantity = item.quantity - 1;
      updateQuantity(item, newQuantity);
    };
    
    return (
      <View style={styles.cartItem}>
        <Image source={{ uri: item.image }} style={styles.cartItemImage} />
        <View style={styles.cartItemDetails}>
          <Text style={styles.cartItemTitle}>{item.title}</Text> 
          <Text style={styles.cartItemPrice}>₪{item.price} x {item.quantity}</Text>
        </View>
        <View style={styles.quantityContainer}>
          <TouchableOpacity onPress={decreaseQuantity} style={styles.quantityButton}>
            <Text style={styles.quantityButtonText}>-</Text>
          </TouchableOpacity>
          <Text style={styles.quantityText}>{item.quantity}</Text>
          <TouchableOpacity onPress={increaseQuantity} style={styles.quantityButton}>
            <Text style={styles.quantityButtonText}>+</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => removeFromCart(item.title)} style={styles.removeButton}>
          <TrashIcon width={24} height={24} fill="white" />
        </TouchableOpacity>
      </View>
    );
  };


  const dataToPass = {
    cart: cart,
    total: calculateTotalCost(),
  };
  return (
    <View style={styles.container}>
      <CustomHeader title={t('screens.cart')} /> 
      <FlatList
        data={cart}
        keyExtractor={(item, index) => index.toString()}
        renderItem={renderCartItem}
        contentContainerStyle={styles.listContainer}
      />
      <View style={styles.totalContainer}>
        <Text style={styles.totalText}>{t('total')}: ₪{calculateTotalCost()}</Text>
      </View>
      <View style={styles.ButtonContainer}>
      <TouchableOpacity 
        style={styles.checkoutButton} 
        onPress={() => { 
          navigation.navigate('Checkout', dataToPass);
        }}
      >
        <Text style={styles.checkoutButtonText}>{t('checkout')}</Text>
      </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F7F7F7',
  },
  listContainer: {
    padding: 16
  },
  cartItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 8,
    padding: 16,
    backgroundColor: '#FFFFFF',
    borderRadius: 12,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 8,
    elevation: 4,
  },
  cartItemImage: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginRight: 16,
  },
  cartItemDetails: {
    flex: 1,
    paddingRight: 30,
    flexDirection: 'column',
    alignContent: 'flex-end',
    alignItems: 'flex-end',
  },
  cartItemTitle: {
    fontSize: 10,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  
  cartItemPrice: {
    fontSize: 10,
    color: '#53B175',
    fontWeight: 'bold',
  },
  quantityContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 16,
  },
  quantityButton: {
    borderWidth: 1,
    borderColor: '#EBEBEB',
    padding: 5,
    width: 30,
    height:30,
    borderRadius: 5,
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  },
  quantityButtonText: {
    fontSize: 12,
    lineHeight: 13
  },
  quantityText: {
    marginHorizontal: 10,
    fontSize: 16,
  },
  removeButton: {
    padding: 8,
    backgroundColor: '#FF3B30',
    borderRadius: 12,
  },
  totalContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 16,
  },
  totalText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  checkoutButton: {
    backgroundColor: '#53B175',
    padding: 16,
    borderRadius: 8,
    alignItems: 'center',
  },
  checkoutButtonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  ButtonContainer:{
    paddingHorizontal: 16,
    paddingBottom: 16
  }
});

export default CartScreen;
