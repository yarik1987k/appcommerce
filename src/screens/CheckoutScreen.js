import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import { useTranslation } from 'react-i18next';
import CustomHeader from '../components/CustomHeader';
import { format } from 'date-fns';
import { enUS, he, ru } from 'date-fns/locale';

const CheckoutScreen = ({ navigation, route }) => {
  const { t, i18n } = useTranslation();
  const [address, setAddress] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [deliveryDate, setDeliveryDate] = useState(null);
  const [errorMessage, setErrorMessage] = useState('');
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const [hasInteracted, setHasInteracted] = useState(false);

  // Extract cartData from route params and set a default value
  const cartData = route.params;

  useEffect(() => {
    if (!hasInteracted) {
      return;
    }

    if (!address) {
      setErrorMessage(t('errors.missingFieldAddress'));
      setIsButtonDisabled(true);
    } else if (!phoneNumber) {
      setErrorMessage(t('errors.missingFieldPhone'));
      setIsButtonDisabled(true);
    } else if (!deliveryDate) {
      setErrorMessage(t('errors.missingFieldDate'));
      setIsButtonDisabled(true);
    } else {
      setErrorMessage('');
      setIsButtonDisabled(false);
    }
  }, [address, phoneNumber, deliveryDate, hasInteracted]);

  const getNextTwoFridays = () => {
    const today = new Date();
    const nextTwoFridays = [];

    for (let i = 1; i <= 14; i++) {
      const date = new Date(today);
      date.setDate(today.getDate() + i);
      if (date.getDay() === 5) {
        nextTwoFridays.push(date);
        if (nextTwoFridays.length === 2) break;
      }
    }

    return nextTwoFridays;
  };

  const normalizeDate = (date) => {
    const normalizedDate = new Date(date);
    normalizedDate.setHours(0, 0, 0, 0);
    return normalizedDate;
  };

  const handleDateSelection = (date) => {
    setDeliveryDate(normalizeDate(date));
    setHasInteracted(true);
    setErrorMessage('');
  };

  const handleCheckout = () => {
    if (!address || !phoneNumber || !deliveryDate) {
      Alert.alert(t('errors.missingFields'));
      return;
    }
    const order = {
      id: '90897',
      placedDate: deliveryDate.toDateString(),
      itemsCount: cartData.cart.length,
      total: cartData.total,
      items: cartData.cart,
      statuses: [
        { label: t('orderPlaced'), date: deliveryDate.toDateString(), completed: true },
        { label: t('orderConfirmed'), date: deliveryDate.toDateString(), completed: true },
        { label: t('orderShipped'), date: deliveryDate.toDateString(), completed: true },
        { label: t('outForDelivery'), date: null, completed: false },
        { label: t('orderDelivered'), date: null, completed: false },
      ],
    };
    navigation.navigate('OrderScreen', { order });
  };

  const handleInputChange = (setter) => (value) => {
    setter(value);
    setHasInteracted(true);
  };

  const nextTwoFridays = getNextTwoFridays();

  const formatDate = (date) => {
    const localeMap = {
      en: enUS,
      he: he,
      ru: ru,
    };
    const locale = localeMap[i18n.language] || enUS;
    return format(date, 'PPPP', { locale });
  };

  return (
    <View style={styles.container}>
      <CustomHeader title={t('screens.checkout')} />  
      <View style={styles.formHolder}>
        <TextInput
          style={styles.input}
          placeholder={t('placeholders.address')}
          value={address}
          onChangeText={handleInputChange(setAddress)}
        />
        <TextInput
          style={styles.input}
          placeholder={t('placeholders.phoneNumber')}
          keyboardType="phone-pad"
          value={phoneNumber}
          onChangeText={handleInputChange(setPhoneNumber)}
        />
        <View style={styles.dateContainer}>
          {nextTwoFridays.map((date, index) => {
            const isSelected = deliveryDate && normalizeDate(deliveryDate).getTime() === normalizeDate(date).getTime();
            return (
              <TouchableOpacity
                key={index}
                style={[styles.dateButton, isSelected && styles.selectedDateButton]}
                onPress={() => handleDateSelection(date)}
              >
                <Text style={[styles.dateText, isSelected && styles.selectedDateText]}>
                  {formatDate(date)}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
        <View>
          {errorMessage ? <Text style={styles.errorMessage}>{errorMessage}</Text> : null}
          <TouchableOpacity
            style={[styles.checkoutButton, isButtonDisabled ? styles.disabledButton : null]}
            onPress={handleCheckout}
            disabled={isButtonDisabled}
          >
            <Text style={styles.checkoutButtonText}>{t('buttons.checkout')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F4F5F9',
  },
  formHolder: {
    padding: 16
  },
  title: {
    fontSize: 24,
    marginBottom: 16,
    textAlign: 'center',
  },
  input: {
    height: 60,
    borderColor: '#F4F5F9',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 16,
    backgroundColor: '#ffffff',
    color: '#868889'
  },
  dateContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 16,
  },
  dateButton: {
    backgroundColor: '#E2E2E2',
    padding: 10,
    borderRadius: 8,
    alignItems: 'center',
    flex: 1,
    marginHorizontal: 5,
  },
  selectedDateButton: {
    backgroundColor: '#53B175',
  },
  dateText: {
    fontSize: 12,
  },
  selectedDateText: {
    color: 'white',
  },
  checkoutButton: {
    backgroundColor: '#53B175',
    padding: 16,
    borderRadius: 8,
    alignItems: 'center',
  },
  checkoutButtonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  errorMessage: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  disabledButton: {
    backgroundColor: '#F2F3F2',
  },
});

export default CheckoutScreen;
