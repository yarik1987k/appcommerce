import React from 'react';
import { View, Text, StyleSheet,Image } from 'react-native';
import { useTranslation } from 'react-i18next';
import CustomHeader from '../components/CustomHeader';

const OrderScreen = ({ route,cart }) => {
  const { t } = useTranslation();
  const orderData = route.params;

  return (
    <View style={styles.container}>
      <CustomHeader title={t('screens.orederReview')}/>
      <View style={styles.orderHeader}>
        <View style={styles.orderIcon}>
          <Text style={styles.orderIconText}>📦</Text>
        </View>
        <View>
          <Text style={styles.orderNumber}>{t('orderNumber')} - { orderData.order.id }</Text>
          <Text style={styles.orderDate}>{t('placedOn')}: { orderData.order.placedDate }</Text>
          <Text>{t('items')} - { orderData.order.itemsCount }</Text>
          <Text>{t('total')}- ₪ { orderData.order.total }</Text>
        </View>
      </View>
      <View style={styles.orderStatus}>
      {orderData.order.items.map((item, index) => (
          <View key={index} style={styles.itemRow}>
                        <View style={styles.imageContainer}>
            <Image source={{ uri: item.image }} style={styles.itemImage} />
            </View>
            <View style={styles.itemDetails}>
              <Text style={styles.itemTitle}>{item.title}</Text>
              <Text style={styles.itemPrice}>{t('price')}: ₪{item.price}</Text>
              <Text style={styles.itemQuantity}>{t('quantity')}: {item.quantity}</Text>
            </View>

          </View>
        ))}

      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F4F5F9',
  },

  orderHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16, 
    padding: 16,
    backgroundColor: 'white'
  },
  orderIcon: {
    backgroundColor: '#E2E2E2',
    padding: 20,
    borderRadius: 50,
    marginRight: 16,
  },
  orderIconText: {
    fontSize: 24,
  },
  orderNumber: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  orderDate: {
    color: 'gray',
  },
  orderStatus: {
    padding: 16,
    backgroundColor: '#FFF',
    borderRadius: 8,
  },
  statusRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
  },
  statusIndicatorContainer: {
    alignItems: 'center',
    marginRight: 16,
  },
  statusIndicator: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: 'gray',
  },
  statusIndicatorCompleted: {
    backgroundColor: '#53B175',
  },
  statusIndicatorPending: {
    backgroundColor: '#E2E2E2',
  },
  statusLine: {
    width: 2,
    height: 30,
    backgroundColor: '#E2E2E2',
  },
  statusTextContainer: {
    flex: 1,
  },
  statusTextCompleted: {
    fontWeight: 'bold',
  },
  statusTextPending: {
    color: 'gray',
  },
  statusDate: {
    color: 'gray',
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    width: '100%',  
  },
  itemDetails:{
    marginRight: 20,
    width: 200,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignContent: 'flex-end',
    alignItems: 'flex-end'
},
imageContainer: {
    width:100
},
  itemImage:{
    width: 100,
    height: 100,
    aspectRatio: 1
  }
});

export default OrderScreen;
