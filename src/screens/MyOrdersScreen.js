// src/screens/MyOrdersScreen.js
import React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import CustomHeader from '../components/CustomHeader';

const MyOrdersScreen = ({ navigation }) => {
  // Mock data for orders
  const orders = [
    { id: '1', date: '2024-05-12', total: '₪100.00', status: 'Delivered' },
    { id: '2', date: '2024-05-10', total: '₪50.00', status: 'Shipped' },
  ];

  const renderOrder = ({ item }) => (
    <View style={styles.orderItem}>
      <Text style={styles.orderText}>Order ID: {item.id}</Text>
      <Text style={styles.orderText}>Date: {item.date}</Text>
      <Text style={styles.orderText}>Total: {item.total}</Text>
      <Text style={styles.orderText}>Status: {item.status}</Text>
    </View>
  );

  return (
    <View style={styles.container}>
      <CustomHeader title="My Orders" />
      <FlatList
        data={orders}
        keyExtractor={(item) => item.id}
        renderItem={renderOrder}
        contentContainerStyle={styles.listContainer}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F5F9',
  },
  listContainer: {
    padding: 16,
  },
  orderItem: {
    backgroundColor: '#FFFFFF',
    padding: 16,
    borderRadius: 8,
    marginBottom: 16,
  },
  orderText: {
    fontSize: 16,
    marginBottom: 4,
  },
});

export default MyOrdersScreen;
