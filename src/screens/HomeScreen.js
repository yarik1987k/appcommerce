import React from 'react';
import { View, Text, FlatList, StyleSheet, Image, TouchableOpacity } from 'react-native';
import CATEGORIES from '../data/categories.json';
import { useTranslation } from 'react-i18next';  
import CustomHeader from '../components/CustomHeader';

function HomeScreen({ navigation }) {
  const { t } = useTranslation();

  return (
    <View style={styles.container}>
      <CustomHeader title={t('categories')}/> 
      <FlatList
        data={CATEGORIES}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity
            style={styles.category}
            onPress={() => navigation.navigate('Category', { categoryId: item.id, categoryName: item.name })}
          >
            <Image source={{ uri: item.imageUrl }} style={styles.image} />
            <Text style={styles.categoryText}>{t(`categoriesData.${item.id}`)}</Text>
          </TouchableOpacity>
        )}
        numColumns={2}
        columnWrapperStyle={styles.columnWrapper}
        contentContainerStyle={styles.contentContainer}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F5F9'
  },
  containerHeader:{
    height: 60,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  },
  title: {
    fontSize: 16, 
    textAlign: 'center', // Center the title
  },
  category: {
    width: '48%',
    padding: 10,
    marginVertical: 8, 
    borderRadius: 18,
    alignItems: 'center',
    borderColor: '#E2E2E2', // Corrected the spelling from 'borderColors'
    backgroundColor: "#FFFFFF",
  },
  categoryText: {
    fontSize: 16,
    marginVertical: 8,
  },
  image: {
    width: 80,
    height: 80,
    aspectRatio: 1 
  },
  columnWrapper: {
    justifyContent: 'space-between',
  },
  contentContainer: {
    paddingHorizontal: 16,
    paddingTop: 16
  },
});

export default HomeScreen;
