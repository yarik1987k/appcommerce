import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import languageDetector from './src/components/languageDetector'; // Import the custom language detector

const resources = {
  en: {
    translation: {
      "onboarding": {
          "title": "Welcome to Our shop!"
      },
      "price": "Price",
      "quantity": "Quantity",
      "placedOn": "Placed On",
      "items": "Items",
      "placeholders": {
        "address": "Address",
        "phoneNumber": "Phone Number"
      },
      "errors":{
        "missingFields": "Missing Fields",
        "fridayOnly": "Friday Only",
        "pastDate": "Past Date",
        "missingFieldPhone": "Missing Field Phone",
        "missingFieldAddress": "Missing Field Address", 
        "missingFieldDate": "Missing date field"
      },
      "orderNumber": "Order Number",
      "categories": "Categories",
      "viewProducts": "View Products",
      "total": "Total",
      "checkout": "Checkout",
      "buttons": {
        "cart": "Cart",
        "addToCart": "Add to cart",
        "goToCart": "Go to cart",
        "getStarted": "Start Shopping",
        "checkout": "Checkout"
      },
      "screens": {
        "home": "Home",
        "category": "Category",
        "cart": "Cart",
        "checkout": "Checkout",
        "orederReview": "Order Review"
      },
      "categoriesData": { 
        "1": "Vegetables",
        "7": "Berries",
        "4": "Sales",
        "2": "Fruits",
        "5": "Herbs and Greens"
      }
    }
  },
  he: {
    translation: {
      "onboarding": {
        "title": "ברוכים הבאים לחנות שלנו!"
    },
    "price": "מחיר",
    "quantity": "כמות",
    "placedOn": "תאריך יצירת הזמנה",
    "items": "כמות פריטים",
    "placeholders": {
      "address": "כתובת למשלוח",
      "phoneNumber": "טלפון למשלוח"
    },
    "errors":{
      "missingFields": "חסרים פרטים בשדות",
      "fridayOnly": "רק ימי שישי",
      "pastDate": "התאריך עבר",
      "missingFieldPhone": "חסר טלפון בשדה",
      "missingFieldAddress": "חסרה כתובת בשדה", 
      "missingFieldDate": "יש לבחור תאריך"
    },
    "orderNumber": "מספר הזמנה",
      "categories": "קטגוריות",
      "viewProducts": "צפה במוצרים",
      "total": "סך הכל",
      "checkout": "לשלוח הזמנה",
      "buttons": {
        "cart": "עגלה",
        "addToCart": "הוסף מוצר לעגלה",
        "goToCart": "לעבור לעגלה",
        "getStarted": "אני רוצה להזמין",
        "checkout": "לשלוח הזמנה"
      },
      "screens": {
        "home": "ראשי",
        "category": "קטגוריה",
        "cart": "עגלה",
        "checkout": "קופה",
        "orederReview": "הזמנה"
      },
      "categoriesData": {
        "4": "מבצעים",
        "7": "פירות יער",
        "1": "ירקות",
        "2": "פירות",
        "5": "עשבי תיבול"
      }
    }
  },
  ru: {
    translation: {
      "onboarding": {
        "title": "Добро пожаловать в наш магазин!"
    },
    "price": "Цена",
    "quantity": "Количество",
    "placedOn": "Дата создание заказа",
    "items": "Количество товаров",
    "placeholders": {
      "address": "Адрес",
      "phoneNumber": "Номер телефона"
    },
    "errors":{
      "missingFields": "Missing Fields",
      "fridayOnly": "Friday Only",
      "pastDate": "Past Date",
      "missingFieldPhone": "Missing Field Phone",
      "missingFieldAddress": "Missing Field Address",
      "missingFieldDate": "Необходимо выбирать дату доставки"
    },
    "orderNumber": "Номер заказа",
      "categories": "Категории",
      "viewProducts": "Просмотреть продукты",
      "total": "Всего",
      "checkout": "Оформить заказ",
      "buttons": {
        "cart": "Корзина",
        "addToCart": "Добавить",
        "goToCart": "Перейти в корзину",
        "getStarted": "К товарам",
        "checkout": "Оформить заказ"
      },
      "screens": {
        "home": "Главная",
        "category": "Категория",
        "cart": "Корзина",
        "checkout": "Оформления заказа",
        "orederReview": "Заказ"
      },
      "categoriesData": {
        "4": "Акции",
        "7": "Ягоды",
        "1": "Овощи",
        "2": "Фрукты",
        "5": "Травы и зелень"
      }
    }
  }
};

const i18nInitPromise = i18n
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    compatibilityJSON: 'v3',
    resources,
    fallbackLng: 'en',
    debug: true,
    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
    react: {
      useSuspense: false,
    },
  });

export { i18nInitPromise };
export default i18n;
