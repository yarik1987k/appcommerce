import React, { useState, useEffect, useRef } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AppNavigator from './src/navigation/Navigation'; 
import { I18nextProvider } from 'react-i18next';
import i18n, { i18nInitPromise } from './i18n';
import { ActivityIndicator, View } from 'react-native';

// Loading component to show while i18next is initializing
const Loading = () => (
  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
    <ActivityIndicator size="large" color="#0000ff" />
  </View>
);

const App = () => {
  const [isI18nInitialized, setIsI18nInitialized] = useState(false);
  const navigationRef = useRef(null);

  useEffect(() => {
    i18nInitPromise.then(() => {
      setIsI18nInitialized(true);
    });
  }, []);

  if (!isI18nInitialized) {
    return <Loading />;
  }

  return (
    <I18nextProvider i18n={i18n}>
      <NavigationContainer ref={navigationRef}>
        <AppNavigator navigationRef={navigationRef} />
      </NavigationContainer>
    </I18nextProvider>
  );
};

export default App;
